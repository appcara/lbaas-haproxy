#!/usr/bin/ruby

require 'yaml'
require 'erb'

$records = []
Dir.glob("/root/haproxy-cfg/*").each do |f| 
  next if File.directory? f
  $records.push(YAML.load_file(f))
end

erb_template = File.read('/root/lbaas-haproxy/haproxy.cfg.erb')
renderer = ERB.new(erb_template, 0, '-')
File.write('/etc/haproxy/haproxy.cfg', renderer.result)